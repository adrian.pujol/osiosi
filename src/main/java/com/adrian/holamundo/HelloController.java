package com.adrian.holamundo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController{

    @RequestMapping(value = "/holiwi", method = RequestMethod.GET)
    @ResponseBody
    public String sayHi(){
        return "Holiwi";
    }

}
